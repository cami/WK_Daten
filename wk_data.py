import requests
import argparse
import datetime
import csv
from tabulate import tabulate

CUR_YEAR = datetime.datetime.now().date().year


def parse_args():
    """
    Parses the arguments from the CLI
    """
    parser = argparse.ArgumentParser(description='Get WK Date')
    parser.add_argument(
        '--type', '-t',
        choices=['json', 'csv'],
        default='json',
        help='defines which format to use for the output (default: json)'
    )
    parser.add_argument(
        '--year', '-y',
        default=CUR_YEAR,
        help='defines the year (default: current year)'
    )
    parser.add_argument(
        '--unit', '-u',
        required=True,
        default="Mech Br Stabsbat 11",
        help="Defines the unit"
    )
    parser.add_argument(
        '--lang', '-l',
        choices=['de', 'fr', 'it'],
        default='de',
        help='Defines de language (default: de)'
    )
    return parser.parse_args()


def get_request(args):
    """
    Creates the request URL and send a get request
    """
    api_url = "https://www.milaufgebotstableau.vbs.admin.ch/v1/wk_entries?from={}-01-01&until={}-12-31&unit={}&type={}&lang={}".format(
        args.year, args.year, args.unit, args.type, args.lang
    )

    response = requests.get(api_url)

    if (args.type == 'csv'):
        res = [row for row in csv.reader(
            response.text.splitlines(), delimiter=';')]
        headers = res[0]
        rows = res[1:]
        print(tabulate(rows, headers=headers))
    else:
        print(response.text)


def main():
    args = parse_args()
    get_request(args)


if __name__ == '__main__':
    main()
