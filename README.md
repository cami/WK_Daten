# WK Daten

Just a small script to get the date for the next military service in Switzerland.

The script uses the following API: https://www.milaufgebotstableau.vbs.admin.ch/index.html

Important information: The dates do not include the KVK-Week

## Usage 

## Before using

Before you can use the script you might need to install some dependencies. The best way will be to create a virtual environment and then install the dependencies. Use the following commands:

```
python -m  venv venv
source venv/bin/activate # the command on Windows systems is a little bit different)
pip install -r requirements.txt
```

### Example usage

if you need the instruction you can enter the following command:

```
python wk_data.py --help
```

Below is an example how to use it with a specified unit, year and type of output

```
python wk_data.py -u "Mech Br Stabsbat 11" -y 2021 -l de -t csv
```

### Docker Usage

If you'd like to use the Docker image then there are two possibilities.

1. You can run the created image in the following way

```
docker run caminsha/wk-data -u "Mech Br Stabsbat 11" -t csv
```

2. You can create your own image with the following command and run the container afterwards

```
docker build -t wk-data .
docker run localhost/wk-data -u "Mech Br Stabsbat 11" -t csv
```

